
real dconv2vm(real x, real ori_pref, real kappa_weights, real kappa_channel){
    return modified_bessel_first_kind(0, sqrt(kappa_weights^2 + kappa_channel^2 + 2 * kappa_weights * kappa_channel * cos(x - ori_pref))) /
  (2 * pi() * modified_bessel_first_kind(0, kappa_weights) * modified_bessel_first_kind(0, kappa_channel));
}

/**
  * Density function of convolution of w von Mises distributions
  * Note, f*(m(g+a)) = m(f*g) + m(f+a), which is involves
  * m(f*g) := m times the convolution of two von mises and
  * m(f*a) := m times the convolution of a von mises and rectangular function
  * so, m(f*a) = m(f*a1) = m(a(f*1)) = m a (f*1)
  * the second one is guessed at with a VM and very low concentration
  *
  * @param x vector of orientations at which to evaluate pdf
  * @param ori_pref center of von mises (channel's preferred orientation)
  * @param kappa_weights concentration of weights
  * @param kappa_channel concentration of NTF
  * @return density vector (response of voxel to ori_tested)
  */
real dconv2vm_pdf(real x, real ori_pref, real kappa_weights, real kappa_channel, real gain, real additive){
  real first = gain * dconv2vm(x, ori_pref, kappa_weights, kappa_channel);
  real second = gain * additive * dconv2vm(x, ori_pref, kappa_weights, 0.01);
  return first + second;
}
