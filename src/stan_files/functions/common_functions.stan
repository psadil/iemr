
vector calc_vox_avg(vector mu, int n_voxel, int max_nv, int n, int[] voxel, vector n_v_real){
  vector[max_nv] vox_sum;
  vector[n_voxel] vox_avg;
  int j;

  for(v in 1:n_voxel){
    vox_sum = rep_vector(0, max_nv);
    j = 1;
    for(i in 1:n){
      if(voxel[i] == v){
        vox_sum[j] = mu[i];
        j += 1;
      }
    }
    vox_avg[v] = sum(vox_sum) / n_v_real[v];
  }
  return vox_avg;
}

real calc_alpha(real mode, real concentration){
  return fma(mode, concentration-2.0, 1.0);
}

real calc_beta(real mode, real concentration){
  return fma(1-mode, concentration-2.0, 1.0);
}
