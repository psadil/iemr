#include /chunks/license.stan
functions{
	// vector von_mises_pdf(vector ori_tested, real ori_pref, real kappa );
#include /functions/channel_responses.stan
}
data {
	int n;
	int n_voxel;
	int n_contrast;
	int n_orientation_tested;
	int contrast[n];
	int voxel[n];
	vector[10] priors;
	vector[n] y;
	matrix[n, n_orientation_tested * n_contrast ] X;
	vector[n_orientation_tested] unique_orientation_tested;
}
transformed data {
	int K = cols(X);
}
parameters {
	real<lower = 0> sigma;
	vector<lower = 0>[n_contrast] m;
	vector<lower = 0>[n_contrast] s;
	vector<lower = 0>[n_contrast] a;
	vector<lower = 0>[n_voxel] weights_kappa;
	vector<lower = -pi(),upper = pi()>[n_voxel] pref_ori;
	vector[n_voxel] v_base;
}
model{
	// prior
	m ~ lognormal(priors[2], priors[3]);
	s ~ lognormal(priors[4], priors[5]);
	a ~ gamma(2, priors[6]);
	weights_kappa ~ gamma(2,priors[7]);

	v_base ~ normal(0, priors[8]);
	sigma ~ gamma(2, priors[9]);
	pref_ori ~ von_mises(0,priors[10]);

	// likelihood
	{
		matrix[n_orientation_tested, n_contrast] channel_resp;
		matrix[K, n_voxel] aW;

		for(v in 1:n_voxel){
			for(con in 1:n_contrast){
			  for(ori in 1:n_orientation_tested){
  				channel_resp[ori, con] = dconv2vm_pdf(unique_orientation_tested[ori], pref_ori[v], weights_kappa[v], s[con], m[con],a[con]);
			  }
			}
			aW[, v] = to_vector(channel_resp);
		}
		y ~ normal(rows_dot_product(X, aW[,voxel]') + v_base[voxel], sigma);
	}
}
